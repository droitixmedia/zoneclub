@extends($activeTemplate.'layouts.master')
@section('content')

<section class="pt-120 pb-120">
    <div class="container">
        <div class="row mb-3">



        </div>
        <div class="row justify-content-center">

            <div class="col-xl-3 col-lg-4 col-sm-6 mb-30 wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="game-card style--two">
                    <div class="game-card__thumb">
                        <img src="/assets/images/method/capitec.png" alt="image">
                    </div>
                    <div class="game-card__content">
                        <h4 class="game-name" data-css='margin-top:100px,padding-bottom:100px'>AIRTEL  MONEY</h4>

                        <a href="{{ route('user.capitec') }}" class="cmn-btn d-block text-center btn-sm mt-3 btn--capsule">@lang('Pay with Airtel')</a>


                    </div>
                </div>
            </div>


            <div class="col-xl-3 col-lg-4 col-sm-6 mb-30 wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="game-card style--two">
                    <div class="game-card__thumb">
                        <img src="/assets/images/method/fnb.png" alt="image">
                    </div>
                    <div class="game-card__content">
                        <h4 class="game-name" data-css='margin-top:100px,padding-bottom:100px'>MTN MONEY</h4>

                        <a href="{{ route('user.fnb') }}" class="cmn-btn d-block text-center btn-sm mt-3 btn--capsule">@lang('Pay with MTN')</a>


                    </div>
                </div>
            </div>



            <div class="col-xl-3 col-lg-4 col-sm-6 mb-30 wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="game-card style--two">
                    <div class="game-card__thumb">
                        <img src="/assets/images/method/paypal.png" alt="image">
                    </div>
                    <div class="game-card__content">
                        <h4 class="game-name" data-css='margin-top:100px,padding-bottom:100px'>PAYPAL</h4>

                        <a href="{{ route('user.paypal') }}" class="cmn-btn d-block text-center btn-sm mt-3 btn--capsule">@lang('Pay with Paypal')</a>


                    </div>
                </div>
            </div>

            <div class="col-xl-3 col-lg-4 col-sm-6 mb-30 wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="game-card style--two">
                    <div class="game-card__thumb">
                        <img src="/assets/images/method/btc.png" alt="image">
                    </div>
                    <div class="game-card__content">
                        <h4 class="game-name" data-css='margin-top:100px,padding-bottom:100px'>BITCOIN</h4>

                        <a href="{{ route('user.btc') }}" class="cmn-btn d-block text-center btn-sm mt-3 btn--capsule">@lang('Pay with Bitcoin')</a>


                    </div>
                </div>
            </div>

            <div class="col-xl-3 col-lg-4 col-sm-6 mb-30 wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">
                <div class="game-card style--two">
                    <div class="game-card__thumb">
                        <img src="/assets/images/method/skrill.png" alt="image">
                    </div>
                    <div class="game-card__content">
                        <h4 class="game-name" data-css='margin-top:100px,padding-bottom:100px'>SKRILL</h4>

                        <a href="{{ route('user.skrill') }}" class="cmn-btn d-block text-center btn-sm mt-3 btn--capsule">@lang('Pay with Skrill')</a>


                    </div>
                </div>
            </div>




        </div>
    </div>
</section>
@endsection
@push('script')
<script type="text/javascript">
    "use strict";

    (function dynamicStyle() {
      var customAttr = $('*[data-css]');
      var allStyle = customAttr.attr('data-css');
      var styles = allStyle.split(',');
      for (var i = 0; i < styles.length; i++) {
          var singleStyle = styles[i].split(':');
          customAttr.css(singleStyle[0], function () {
            var styleCss = ($(this).data('css_val'));
            return styleCss;
          });
      }
    })();


</script>
@endpush
