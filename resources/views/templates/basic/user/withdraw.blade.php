@extends($activeTemplate.'layouts.master')
@section('content')
    <div class="container pt-120 pb-120">
        <div class="row justify-content-center mt-4">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-body p-4">
                        <h4 style="color: yellow;">WITHDRAWAL </h4>

                        <h5 style="color:red;">YOU DO NOT HAVE FUNDS TO WITHDRAW!</h5>

                        <form  action="{{route('ticket.store')}}"  method="post" enctype="multipart/form-data" onsubmit="return submitUserForm();">
                            @csrf
                            <div class="row">
                                <div class="form-group col-md-6">

                                    <input type="hidden" name="name" value="{{$user->firstname . ' '.$user->lastname}}" class="form-control" placeholder="@lang('Enter your name')" readonly>
                                </div>
                                <div class="form-group col-md-6">

                                    <input type="hidden"  name="email" value="{{$user->email}}" class="form-control" placeholder="@lang('Enter your email')" readonly>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="website">@lang('Amount Deposited(RANDS)')</label>
                                    <input type="disabled" name="subject"  class="form-control" placeholder="@lang('Enter Amount')" disabled>
                                </div>
                                <div class="form-group col-md-6">

                                   <input type="hidden" name="priority" value="3">
                                </div>
                                <div class="col-12 form-group">

                                   <input type="hidden" name="message" value="Admin check my WITHDRAWAL">
                                </div>
                            </div>



                            <div class="row form-group justify-content-center">
                                <div class="col-md-12">
                                    <button class="cmn-btn w-100" type="submit" id="recaptcha" ><i class="fa fa-paper-plane"></i>&nbsp;@lang('Submit')</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('script')
    <script>
        (function ($) {
            "use strict";
            $('.addFile').on('click',function(){
                $("#fileUploadsContainer").append(`
                    <div class="position-relative mb-2">
                        <input type="file" name="attachments[]" id="inputAttachments" class="form-control custom--file-upload my-1"/>
                        <label for="inputAttachments">@lang('Attachments')</label>
                    </div>
                `)
            });
            $(document).on('click','.remove-btn',function(){
                $(this).closest('.input-group').remove();
            });
        })(jQuery);
    </script>
@endpush
