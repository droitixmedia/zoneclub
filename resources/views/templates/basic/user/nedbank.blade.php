@extends($activeTemplate.'layouts.master')
@section('content')
    <div class="container pt-120 pb-120">
        <div class="row justify-content-center mt-4">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-body p-4">
                        <h4 style="color: yellow;">NEDBANK PAYMENT INSTRUCTIONS</h4><u>
                        <h5>BANK: NEDBANK</h5>

                        <h5>ACCOUNT NUMBER: 1215252617</h5>
                       </u>
                        <h5 style="color:red;">**After depositing to the above account details,just fill in the form below and your deposit will be approved**</h5>
                         <h5 style="color:green">YOU CAN SEND A WHATSAPP MESSAGE TO 0639748635 IF YOU NEED QUICK APPROVAL</h5>
                         <h3>MINIMUM: R300</h3>
                         <h3>MAXIMUM: R100000</h3>
                        <form  action="{{route('ticket.store')}}"  method="post" enctype="multipart/form-data" onsubmit="return submitUserForm();">
                            @csrf
                            <div class="row">
                                <div class="form-group col-md-6">

                                    <input type="hidden" name="name" value="{{$user->firstname . ' '.$user->lastname}}" class="form-control" placeholder="@lang('Enter your name')" readonly>
                                </div>
                                <div class="form-group col-md-6">

                                    <input type="hidden"  name="email" value="{{$user->email}}" class="form-control" placeholder="@lang('Enter your email')" readonly>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="website">@lang('Amount Deposited(RANDS)')</label>
                                    <input type="text" name="subject"  class="form-control" placeholder="@lang('Enter Amount')">
                                </div>
                                <div class="form-group col-md-6">

                                   <input type="hidden" name="priority" value="3">
                                </div>
                                <div class="col-12 form-group">

                                   <input type="hidden" name="message" value="Admin check my NEDBANK deposit">
                                </div>
                            </div>

                            <div class="row form-group ">
                                <div class="col-sm-9 file-upload">
                                    <div class="position-relative">
                                        <input type="file" name="attachments[]" id="inputAttachments" class="form-control custom--file-upload my-1"/>
                                        <label for="inputAttachments">@lang('PROOF OF PAYMENT')</label>
                                    </div>
                                    <p class="ticket-attachments-message text-muted mb-3">
                                        @lang("Allowed File Extensions: .jpg, .jpeg, .png, .pdf, .doc, .docx")
                                    </p>
                                    <div id="fileUploadsContainer"></div>
                                </div>


                            </div>

                            <div class="row form-group justify-content-center">
                                <div class="col-md-12">
                                    <button class="cmn-btn w-100" type="submit" id="recaptcha" ><i class="fa fa-paper-plane"></i>&nbsp;@lang('Submit')</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('script')
    <script>
        (function ($) {
            "use strict";
            $('.addFile').on('click',function(){
                $("#fileUploadsContainer").append(`
                    <div class="position-relative mb-2">
                        <input type="file" name="attachments[]" id="inputAttachments" class="form-control custom--file-upload my-1"/>
                        <label for="inputAttachments">@lang('Attachments')</label>
                    </div>
                `)
            });
            $(document).on('click','.remove-btn',function(){
                $(this).closest('.input-group').remove();
            });
        })(jQuery);
    </script>
@endpush
